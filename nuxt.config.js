module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: process.env.npm_package_description || ''}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {rel: "stylesheet", href: "https://cdn.bootcdn.net/ajax/libs/Swiper/5.4.2/css/swiper.min.css"},
    ],
    script: [
      {
        type: "text/javascript",
        src: "https://cdn.bootcdn.net/ajax/libs/Swiper/5.4.2/js/swiper.min.js"
      },
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: {color: '#fff'},
  /*
  ** Global CSS
  */
  css: [
    "~/assets/css/reset.scss",
    "~/assets/css/common.scss",
    "ant-design-vue/dist/antd.css",
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/antd-ui',
    '@/plugins/axios',
    '@/plugins/babel-plugin',
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [],
  /*
  ** Nuxt.js modules
  */
  modules: [],
  /*
  ** Build configuration
  */
  build: {
    extractCSS: {allChunks: true},
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
