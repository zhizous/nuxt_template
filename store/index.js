// store/index.js
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const store = () => new Vuex.Store({
  state: {
    userInfo: null,
  },
  mutations: {
    userInfo(state, val) {
      state.userInfo = val;
    }
  },
  actions: {
    userInfo(store, callback) {

    }
  }
});

export default store;
