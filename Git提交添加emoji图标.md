@[TOC]
# Git 提交代码时添加 emoji 图标

使用git的开发者都知道提交代码的最简单命令： ```git commit -m '此次提交的内容说明'```。
我们在github发现了这样一张视图：

![](1-git-emoji.png)

这是在commit时，添加了emoji表情说明，我们来看看其命令语法：


## 在commit时添加一个emoji表情图标
```git
git commit -m ':emoji: 此次提交的内容说明'
```

## 添加多个emoji表情图标

```git
git commit -m ':emoji1: :emoji2: :emoji3: 此次提交的内容说明'
```

## 例子

```git
git commit -m ':art: :zap: :bug: 测试emoji'
```

在提交内容的前面增加了emoji标签：  **:emoji:**，其中emoji是表情图标的标签，列表见下面的附录表格。

<p> :apple: :lemon: :tangerine: :pear: :banana: :pineapple: :cherries: :cake: :grapes: :watermelon: :strawberry: :corn: :peach: :melon: :chestnut: </p>

|emoji|	emoji代码	|commit 说明
|----|----|----|
|:tada: (庆祝)	|```:tada:```	|初次提交
|:zap: (闪电)|	```:zap:```	|提升性能
|:racehorse: (赛马)|	```:racehorse:```	|提升性能
|:fire: (火焰)|	```:fire:```|	移除代码或文件
|:bug: (bug)|	```:bug:```	|修复 bug
|:construction: (施工)|	```:construction:```	|工作进行中
|:see_no_evil: (gitignore)|	```:see_no_evil:```	|添加或更新.gitignore文件
|:hammer: (锤子)|	```:hammer:```	|重大重构
|:heavy_minus_sign: (减号)|	```:heavy_minus_sign:```|	减少一个依赖
|:heavy_plus_sign: (加号)|	```:heavy_plus_sign:```	|增加一个依赖
|:package: (包)|	```:package:```|	添加或更新编译的文件或包
|:art: (调色板)|	```:art:```	|改进代码结构/代码格式
|:wrench: (扳手)	|```:wrench:```	|修改配置文件
|:dizzy: (动画过渡)|	```:dizzy:```	|添加或更新动画和过渡
|:loud_sound: (声音)|	```:loud_sound:```	|添加或更新日志
|:mute: (静音)|	```:mute:```	|删除日志
|:ambulance: (急救车)|	```:ambulance:```|	重要补丁
|:sparkles: (火花)|	```:sparkles:```	|引入新功能
|:memo: (备忘录)	|```:memo:```	|撰写文档
|:rocket: (火箭)	|```:rocket:```	|部署功能
|:lipstick: (口红)	|```:lipstick:```	|更新 UI 和样式文件
|:white_check_mark: (白色复选框)	|```:white_check_mark:```	|增加测试
|:lock: (锁)	|```:lock:```	|修复安全问题
|:apple: (苹果)	|```:apple:```|	修复 macOS 下的问题
|:penguin: (企鹅)|	```:penguin:```|	修复 Linux 下的问题
|:checkered_flag: (旗帜)|	```:checkered_flag:```	|修复 Windows 下的问题
|:bookmark: (书签)|	```:bookmark:```	|发行/版本标签
|:rotating_light: (警车灯)|	```:rotating_light:```	|移除 linter 警告
|:green_heart: (绿心)	|```:green_heart:```	|修复 CI 构建问题
|:arrow_down: (下降箭头)	|```:arrow_down:```	|降级依赖
|:arrow_up: (上升箭头)|	```:arrow_up:```	|升级依赖
|:construction_worker: (工人)|	```:construction_worker:```	|添加 CI 构建系统
|:chart_with_upwards_trend: (上升趋势图)|	```:chart_with_upwards_trend:```	|添加分析或跟踪代码
|:whale: (鲸鱼)	|```:whale:```	|Docker 相关工作
|:globe_with_meridians: (地球)|	```:globe_with_meridians:```|	国际化与本地化
|:pencil2: (铅笔)|	```:pencil2:```|	修复 typo
|:children_crossing: (用户)|	```:children_crossing:```|	改善用户体验/可用性
|:egg: (彩蛋)|	```:egg:```|	添加或更新复活节彩蛋


参考资料 :
- https://gitmoji.carloscuesta.me/

