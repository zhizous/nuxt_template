/**
 * process.env.NODE_ENV
 *    development：值为代表开发环境
 *    production：值为代表生产环境
 */

export default function (app) {
   let axios = app.$axios;
   let baseURL = `http://localhost:${app.env.PORT}/api`;
   if (process.client) { //当前状态是客户端
      //`${window.location.origin}`/api
      baseURL = `${window.location.origin}/api`;
      console.log("客户端环境：" + baseURL);
   } else {
      let protocol = process.env.NODE_ENV === "development" ? "http://" : "https://";
      baseURL = protocol + app.req.headers.host + "/api";
      console.log("服务端环境：" + baseURL);
   }
}
